# Calculating and Visualizing Solar System Barycenter

**Project Demo** - [SSB YouTube Demo Video](https://www.youtube.com/watch?v=KZNxCRIVjAk)

**Project Website on Heroku** - [Project Site Codespace](https://codespacehack.herokuapp.com/)

The Sun as we know our source of natural light is not the center point of our solar system. The Sun also revolves around a certain point in the solar system which is the point of center of gravity of solar system. Sun contains the majority of the mass in our solar system as compared to the rest of celestial objects in our solar system, the credit for almost 99% of the mass of the solar system goes to the Sun.

Due to this, the point of center of gravity is shifted very close to the Sun, which makes it appear that all celestial objects in our solar system revolve around the Sun, with Sun being stationary in reference to our solar system. But this point of center of gravity is not always coinciding with the center of Sun and because of it, Sun also shows some slight movement of revolution around a point in space.

This point of center of gravity is called as Solar System Barycenter (SSB)

In this project we are going to calculate the location of SSB in reference to Earth's plane and with respect to the Sun's center.
The project plots the SSB distance from the Sun's center in kilometers at a given point of date and time which is taken from the user.

This webpage is developed using Django webframework for Python.
For frontend HTML, CSS and Jinja templating are used.

This project makes extensive use of SpiceyPy module which is a Python wrapper for SPICE toolkit. The SPICE toolkit is provided by the National Aeronautics and Space Administration (NASA) The Navigation and Ancillary Information Facility (NAIF) project. https://naif.jpl.nasa.gov/naif/

It provides information system to assist scientists in planning and interpreting scientific observations from space-based instruments aboard robotic planetary spacecraft. SPICE is also used in support of engineering tasks associated with these missions. While planetary missions were the original focus, today SPICE is also used on some heliophysics and earth science missions.

SPICE toolkits come with multiple kernels which is used in this project for calculations purposes. The three kernels used in this project are de440.bsp, naif0012.tls and pck00010.tpc under MIT license. 

The project flow is as follows
First the user is asked to provide a year, a month and number of days.
The number of days will be used to calculate the ending year for calculation. Initially all the date and time are in Coordinated Universal Time (UTC). These computations are done by the Python Datetime library.

The UTC time needs to be converted into corresponding Ephemeris Time. This is done using spice function utc2et.

We are ignoring the extra time added between the start_year and end_year because of leap years. This gives us a very slightly varying time difference which is insignificant for our calculations.

Next we compute the SSB position in three dimensions coordinates x, y, and z with respect to Sun's center. The Spice function spkgps is used. The function is given parameters

1. targ: The NAIF ID of the SSB (0).
2. et: The Ephermeris time
3. ref: The reference frame of interest. In our case we compute the position in ECLIPJ2000. So, the ecliptic plane of our home planet is the reference plane.
4. obs: The NAIF ID of the Sun (10).

spkgps returns the position of the SSB w.r.t the Sun at a certain Ephemeris time computed in the ECLIPJ2000 reference frame. The frame is as observed by a person on Earth.

The distance between the SSB and Sun is calculated at the initial time provided by the user and displayed a numerical value in kilometers.

For further computation we need the radius of the Sun. We are using the Sun's radius as 696,000 km as in SPPICE kernel pck00010.tpc and as set by the International Astronomical Union. With the function bodvcd we can extract the radius of the Sun with 3 parameters-

1. bodyid: The NAIF ID of the Sum (10)
2. item: RADII tells the function that we need the radius information of the Sun.
3. maxn: The number of expected return parameters. Since we get the radius values of an ellipsoid (x,y,z direction) the number is set to 3.

We plot the Sun and the trajectory of the SSB. We use the Plotly to generate interactive plots for this task. We create a 2-dimensional projection of our results and plot only the x-y coordinates (movement on the Ecliptic plane).
The plot shows the trajectory in the blue and one can easily see the SSB is not within the Sun all the time (although the Sun contains over the 99% of the Solar System's mass).

A better view is provided with the 3D graph plotted using Plotly for much better interactive experience and studying the path of SSB inside the Sun's surface but still not coinciding with the center of the Sun.
This means that even thought the SSB point might be inside the Sun's surface but as long as it is not coinciding with the center of the Sun, the Sun has to move under the influence of other celestial objects around that center of gravity, SSB.

Hoping this project provides a better understanding of gravity influences in our Solar System and educates people about Astromy and Programming.
