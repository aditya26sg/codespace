from django.shortcuts import render
from django.http.response import JsonResponse
from django.views.generic import TemplateView
from .forms import CalculateSSB
from .task_functions.ssbplot_2D import display_ssb

# Create your views here.
class SolarSystemBarycenter(TemplateView):
    template_name = 'index.html'
    def get(self, request):
        form = CalculateSSB()
        args = {
            'form': form
        }
        return render(request, self.template_name, args)
    def post(self, request):
        form = CalculateSSB(request.POST)
        if form.is_valid():
            initial_year = request.POST['initial_year']
            initial_month = request.POST['initial_month']
            number_of_days = request.POST['number_of_days']
            plot_ssb_type = request.POST['plot_ssb_type']
            plot_html, ssb_dist = display_ssb(initial_year, initial_month, number_of_days, plot_ssb_type)
            args = {
                'form': form,
                'plot_html': plot_html,
                'ssb_dist': ssb_dist
            }
            return render(request, self.template_name, args)