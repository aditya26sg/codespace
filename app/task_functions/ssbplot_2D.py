import numpy as np
import spiceypy as sp
import datetime as dt
import plotly.graph_objects as go
from plotly.offline import plot
import matplotlib.pyplot as plt


def ssb_distance_from_sun(ssb_wrt_sun_position_scaled):
    ssb_wrt_sun_position_scaled_xy = ssb_wrt_sun_position_scaled[:, 0:2]

    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=[ssb_wrt_sun_position_scaled_xy[0, 0],
            ssb_wrt_sun_position_scaled_xy[-1, 0]],
        y=[ssb_wrt_sun_position_scaled_xy[0, 1],
            ssb_wrt_sun_position_scaled_xy[-1, 1]],
        text=["Sun"],
        mode="text",
    ))

    fig.update_xaxes(range=[min(ssb_wrt_sun_position_scaled_xy[:, 0]), max(
        ssb_wrt_sun_position_scaled_xy[:, 0])], zeroline=False)
    fig.update_yaxes(range=[min(ssb_wrt_sun_position_scaled_xy[:, 1]), max(
        ssb_wrt_sun_position_scaled_xy[:, 1])])

    fig.add_trace(go.Scatter(x=ssb_wrt_sun_position_scaled_xy[:, 0], y=ssb_wrt_sun_position_scaled_xy[:, 1],
                             mode='lines',
                             name='BaryCenter',
                             line_color="Blue"))

    fig.update_layout(width=800, height=800,
                      plot_bgcolor='rgba(0,0,0,0)', paper_bgcolor='rgba(0,0,0,0)')
    fig.update_yaxes(automargin=True, color="#FFFFFF")
    fig.update_xaxes(automargin=True, color="#FFFFFF")
    fig['layout']['yaxis'].update(autorange=True)
    fig['layout']['xaxis'].update(autorange=True)

    fig.add_shape(type="circle",
                  xref="x", yref="y", opacity=0.6,
                  x0=-1.0, y0=-1.0, x1=1.0, y1=1.0,
                  line_color="Red",
                  fillcolor="Yellow")

    plot_html = plot(fig, output_type='div',
                     include_plotlyjs=False, show_link=False)
    return plot_html


def ssb_distance_from_sun_3d(ssb_wrt_sun_position_scaled):
    ssb_wrt_sun_position_scaled_xy = ssb_wrt_sun_position_scaled[:, 0:2]
    theta = np.linspace(0, 2*np.pi, 100)
    phi = np.linspace(0, np.pi, 100)

    x0 = np.outer(np.cos(theta), np.sin(phi))
    y0 = np.outer(np.sin(theta), np.sin(phi))
    z0 = np.outer(np.ones(100), np.cos(phi))

    fig = go.Figure(data=go.Surface(x=x0, y=y0, z=z0, opacity=0.6))
    fig.update_xaxes(range=[min(ssb_wrt_sun_position_scaled_xy[:, 0]), max(ssb_wrt_sun_position_scaled_xy[:, 0])], zeroline=False)
    fig.update_yaxes(range=[min(ssb_wrt_sun_position_scaled_xy[:, 1]), max(ssb_wrt_sun_position_scaled_xy[:, 1])])

    # Plotting the barycenter in 3D
    fig.add_trace(go.Scatter3d(x=ssb_wrt_sun_position_scaled_xy[:, 0], y=ssb_wrt_sun_position_scaled_xy[:, 1], z = np.zeros(len(ssb_wrt_sun_position_scaled_xy)),
                    mode='lines',
                    name='BaryCenter',
                    line_color="Blue"))

    fig.update_layout(title = "Solar System Barycenter path wrt SUN",
                     width=800, height=800, plot_bgcolor = 'rgba(0,0,0,0)', paper_bgcolor = 'rgba(0,0,0,0)'
                    )

    plot_html = plot(fig, output_type='div', include_plotlyjs=False, show_link=False)
    return plot_html

def ssb_wrt_sun_radius(start_year, end_year, delta_days, norm_dist):
    layout = go.Layout(
        title = "Solar System Barycenter Distance wrt Sun with time",
        xaxis = dict(
            title="Time",
            color = "#FFFFFF"
        ),
        yaxis = dict(
            title="SSB distance in Sun's radii",
            color = "#FFFFFF"
        ),
        plot_bgcolor = 'rgba(0,0,0,0)', paper_bgcolor = 'rgba(0,0,0,0)'
    )
    fig = go.Figure(layout = layout)
    x = np.linspace(start_year, end_year, delta_days)
    fig.add_trace(go.Scatter(x=x, y=norm_dist,
                             mode='lines',
                             name='BaryCenter',
                             line_color="Blue"))

    
    plot_html = plot(fig, output_type='div', include_plotlyjs=False, show_link=False)
    return plot_html


def display_ssb(year, month, number_of_days, plot_ssb_type):
    sp.furnsh("app/task_functions/_kernels/naif0012.tls")
    sp.furnsh("app/task_functions/_kernels/de440.bsp")
    sp.furnsh("app/task_functions/_kernels/pck00010.tpc")

    init_utc_time = dt.datetime(year=int(year), month=int(
        month), day=1, hour=0, minute=0, second=0)
    delta_period = int(number_of_days)
    end_utc_time = init_utc_time + dt.timedelta(days=delta_period)

    init_utc_time_str = init_utc_time.strftime('%Y-%m-%dT%H:%M:%S')
    end_utc_time_str = end_utc_time.strftime('%Y-%m-%dT%H:%M:%S')
    end_year = int(end_utc_time_str[:4])
    start_year = int(year)

    init_et_time = sp.utc2et(init_utc_time_str)
    end_et_time = sp.utc2et(end_utc_time_str)

    time_et_intervals = np.linspace(init_et_time, end_et_time, delta_period)
    ssb_wrt_sun_position = []

    for TIME_INTERVAL_ET_f in time_et_intervals:
        _position, _ = sp.spkgps(targ=0, et=TIME_INTERVAL_ET_f,
                                 ref='ECLIPJ2000',
                                 obs=10)

        ssb_wrt_sun_position.append(_position)

    ssb_wrt_sun_position = np.array(ssb_wrt_sun_position)

    ssb_dist_sun_center = round(np.linalg.norm(ssb_wrt_sun_position[0]))

    _, sun_radii = sp.bodvcd(bodyid=10, item='RADII', maxn=3)

    sun_radius = sun_radii[0]

    ssb_wrt_sun_position_scaled = ssb_wrt_sun_position / sun_radius
    ssb_wrt_sun_scaled_dist = []
    for x in ssb_wrt_sun_position_scaled:
        ssb_wrt_sun_scaled_dist.append(np.linalg.norm(x))

    if plot_ssb_type == "SSB with Sun 2D":
        plothtml = ssb_distance_from_sun(ssb_wrt_sun_position_scaled)
    elif plot_ssb_type == "SSB with Sun 3D":
        plothtml = ssb_distance_from_sun_3d(ssb_wrt_sun_position_scaled)
    else:
        plothtml = ssb_wrt_sun_radius(start_year, end_year, delta_period,ssb_wrt_sun_scaled_dist)

    return plothtml, ssb_dist_sun_center
