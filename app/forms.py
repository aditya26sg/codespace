from django import forms
from django.forms import widgets
from django.forms.forms import Form
from django import forms
import datetime as dt

plot_types = [
    ('SSB dist graph', 'SSB dist graph'),
    ('SSB with Sun 2D', 'SSB with Sun 2D'),
    ('SSB with Sun 3D', 'SSB with Sun 3D')
]

class CalculateSSB(forms.Form):
    initial_year = forms.IntegerField()
    initial_month = forms.IntegerField()
    number_of_days = forms.IntegerField()
    plot_ssb_type = forms.CharField(label="Plot Type", widget=forms.Select(choices=plot_types))
